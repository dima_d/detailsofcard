package kris.com.detailsofcard

import android.app.Application

import com.raizlabs.android.dbflow.config.FlowManager

class CardApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        FlowManager.init(this)
    }
}
