package kris.com.detailsofcard.mainActivity

import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import net.grandcentrix.thirtyinch.TiPresenter

/**
 * TODO: Add javadoc
 */

class MainPresenter : TiPresenter<IMainActivity>() {
    private val featureManager = FeatureManager()
    private val fragment = BehaviorSubject.createDefault<Feature>(featureManager.getById(-1))!!
    @Volatile var disposable: Disposable? = null

    override fun onAttachView(view: IMainActivity) {
        super.onAttachView(view)
        disposable = fragment
                .distinctUntilChanged()
                .subscribe { view.showFragment(it.fragment.invoke(), it.id) }
    }

    override fun onDetachView() {
        super.onDetachView()
        disposable?.takeIf { !it.isDisposed }?.dispose()
    }

    fun onSelectNavItem(it: Int) = fragment.onNext(featureManager.getById(it))

}