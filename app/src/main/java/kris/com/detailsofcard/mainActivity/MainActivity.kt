package kris.com.detailsofcard.mainActivity

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kris.com.detailsofcard.R
import net.grandcentrix.thirtyinch.TiActivity
import net.grandcentrix.thirtyinch.TiView

interface IMainActivity:TiView{
    fun showFragment(fragment: Fragment, id: Int)
}


class MainActivity : TiActivity<MainPresenter, IMainActivity>(), IMainActivity {

    companion object {
        private val PERMISSIONS_REQUEST = 100
    }

    override fun providePresenter() = MainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        requestPermissions(Manifest.permission.READ_SMS)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener {
            presenter.onSelectNavItem(it.itemId)
            drawer_layout.closeDrawer(GravityCompat.START)
            true
        }
    }

    override fun showFragment(fragment: Fragment, id: Int) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit()
        nav_view?.setCheckedItem(id)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }


    fun requestPermissions(vararg permissions: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, PERMISSIONS_REQUEST);
        }
    }
}
