package kris.com.detailsofcard.mainActivity

import android.support.v4.app.Fragment
import kris.com.detailsofcard.R
import kris.com.detailsofcard.features.details.DetailsFragment
import kris.com.detailsofcard.features.settings.SettingsFragment
import kris.com.detailsofcard.features.summary.SummaryFragment


class FeatureManager {

    private val features = listOf(
            Feature(R.id.startFragment, { SummaryFragment() }),
            Feature(R.id.detailsFragment, { DetailsFragment() }),
            Feature(R.id.settings, { SettingsFragment() })
    )

    fun getById(id: Int): Feature = features.firstOrNull { it.id == id } ?: features[0]
}

class Feature(val id: Int, val fragment: () -> Fragment)
