package kris.com.detailsofcard.features.details

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_details.*
import kris.com.detailsofcard.R
import net.grandcentrix.thirtyinch.TiFragment
import net.grandcentrix.thirtyinch.TiView

interface IDetailsFragment : TiView

class DetailsFragment() : TiFragment<DetailsPresenter,IDetailsFragment>(), IDetailsFragment {
    override fun providePresenter(): DetailsPresenter = DetailsPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.layoutManager = LinearLayoutManager(activity)
        list.adapter = presenter.adapter
    }

}