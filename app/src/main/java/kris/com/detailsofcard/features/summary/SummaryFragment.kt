package kris.com.detailsofcard.features.summary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kris.com.detailsofcard.R
import net.grandcentrix.thirtyinch.TiFragment
import net.grandcentrix.thirtyinch.TiView


interface IMainFragment : TiView

class SummaryFragment : TiFragment<SummaryPresenter, IMainFragment>(), IMainFragment {
    override fun providePresenter(): SummaryPresenter = SummaryPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }
}