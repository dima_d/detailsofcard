package kris.com.detailsofcard.features.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kris.com.detailsofcard.R
import kris.com.detailsofcard.features.details.SettingsPresenter
import net.grandcentrix.thirtyinch.TiFragment
import net.grandcentrix.thirtyinch.TiView

interface ISettingsFragment : TiView

class SettingsFragment() : TiFragment<SettingsPresenter, ISettingsFragment>(), ISettingsFragment {
    override fun providePresenter(): SettingsPresenter = SettingsPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}