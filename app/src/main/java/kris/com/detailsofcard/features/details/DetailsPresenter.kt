package kris.com.detailsofcard.features.details

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.tassta.flex.utils.service.BoundService
import kris.com.detailsofcard.R
import kris.com.detailsofcard.service.MainService
import kris.com.model.CardOperation
import net.grandcentrix.thirtyinch.TiPresenter
import kotlin.coroutines.experimental.coroutineContext

class DetailsPresenter(context: Context):TiPresenter<IDetailsFragment>() {
    val service = BoundService.connector(context, MainService::class.java)
    val adapter = DetailsAdapter()

    override fun onCreate() {
        super.onCreate()
    }

    override fun onDestroy() {
        super.onDestroy()
        service.complete()
    }

}


class DetailsViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

class DetailsAdapter():RecyclerView.Adapter<DetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder =
            DetailsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_details, parent, false))

    override fun getItemCount(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}