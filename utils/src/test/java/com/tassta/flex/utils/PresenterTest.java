package com.tassta.flex.utils;

import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class PresenterTest {

    @Test
    public void lifecycleTest() throws Exception {
        PublishSubject<Integer> tester = PublishSubject.create();

        LifeCycle lifeCycle = new LifeCycle();

        TestObserver<Integer> test = tester
                .compose(lifeCycle.bindToLifecycle())
                .test();

        tester.onNext(0);
        test.assertValue(0);

        lifeCycle.stop();
        tester.onNext(0);
        lifeCycle.stop();
        tester.onNext(0);

        test.assertValueCount(1);
        test.assertComplete();
    }

    @Test
    public void presenterTest() throws Exception {
        PresenterInstance presenterInstance = new PresenterInstance();

        presenterInstance.emitter.onNext(0);

        Object view1 = new Object();
        TestObserver<Integer> test1 = presenterInstance.bindView(view1).test();

        Object view2 = new Object();
        TestObserver<Integer> test2 = presenterInstance.bindView(view2).test();

        Object view3 = new Object();
        TestObserver<Integer> test3 = presenterInstance.bindView(view3).test();

        presenterInstance.emitter.onNext(1);
        presenterInstance.unbindView(view1);

        presenterInstance.emitter.onNext(2);
        presenterInstance.unbindView(view1);
        presenterInstance.unbindView(view2);

        test1.assertComplete();
        test2.assertComplete();
        test3.assertNotComplete();


        presenterInstance.emitter.onNext(3);
        presenterInstance.stop();

        presenterInstance.emitter.onNext(4);

        test1.assertValueCount(2)
                .assertComplete();

        test2.assertValueCount(3)
                .assertComplete();

        test3.assertValueCount(4)
                .assertComplete();
    }

    private class PresenterInstance extends Presenter {

        BehaviorSubject<Integer> emitter = BehaviorSubject.create();

        Observable<Integer> bindView(Object view) {
            return emitter
                    .compose(bindToViewLifecycle(view));
        }
    }
}