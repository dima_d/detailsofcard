package com.tassta.flex.utils

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.junit.Test
import java.util.concurrent.TimeUnit

class RxUtilsKtTest {
    private val TAG = RxUtilsKtTest::class.java.simpleName

    @Test
    fun repeatIn() {
        Observable.error<Exception>(Exception("ex"))
                .doOnError { System.out.println(it)}
                .doOnComplete { System.out.println("complete")}
                .retryWhen(repeatIn(1,2))
                .test()
                .await()
                .assertNoErrors()


    }

    @Test
    fun repeatIn1() {
        val lc = LifeCycle()

        Observable.timer(2, TimeUnit.SECONDS).take(1).subscribe{lc.stop()}

        Observable.error<Exception>(Exception("ex"))
                .doOnError { System.out.println(it)}
                .doOnComplete { System.out.println("complete")}
                .doOnDispose{ System.out.println("dispose")}
                .retryWhen(repeatIn(1,lc.bindToLifecycle()))
                .test()
                .await()
                .assertNoErrors()

    }

    @Test
    fun repeatIn2() {
        val lc = PublishSubject.create<Long>()

        Observable.timer(2, TimeUnit.SECONDS).take(1).subscribe{lc.onNext(0)}

        Observable.error<Exception>(Exception("ex"))
                .doOnError { System.out.println(it)}
                .doOnComplete { System.out.println("complete")}
                .doOnDispose{ System.out.println("dispose")}
                .retryWhen(repeatIn(4,lc))
                .test()
                .await()
                .assertNoErrors()


    }

    @Test
    fun immediately() {
    }

    @Test
    fun immediately1() {
    }

    @Test
    fun immediately2() {
    }
}