package com.tassta.flex.utils;

import java.util.ArrayDeque;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public class IterableUtils {

    public static <T> void foreach(Iterable<T> iterable, Consumer<? super T> action) {

        for (T t : iterable) {
            action.accept(t);
        }
    }

    @SafeVarargs
    public static <T> ObservableSource<? extends T> notNullArray(T... ts) {
        if (ts.length == 0)
            return Observable.empty();

        if (ts.length == 1)
            if (ts[0] == null)
                return Observable.empty();
            else
                return Observable.just(ts[0]);

        ArrayDeque<T> array = new ArrayDeque<>();
        for (T t : ts) {
            if (t != null)
                array.add(t);
        }

        return Observable.fromIterable(array);
    }

    public interface Consumer<T> {

        void accept(T t);
    }
}
