package com.tassta.flex.utils.service

import android.os.Binder

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
internal class ServiceBinder(val service: BoundService) : Binder()
