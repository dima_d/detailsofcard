package com.tassta.flex.utils.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder

import io.reactivex.ObservableTransformer

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
open class BoundService : Service() {
    private val binder = ServiceBinder(this)

    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    companion object {

        fun <T : BoundService> connector(context: Context, service: Class<T>): IServiceConnector<T> {
            return ServiceConnector(context, service)
        }

        fun <T : BoundService> connector(context: Context, service: Class<T>, lifecycle: ObservableTransformer<T, T>): IServiceConnector<T> {
            return ServiceConnector(context, service, lifecycle)
        }
    }


}
