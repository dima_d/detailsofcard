package com.tassta.flex.utils

import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

open class LifeCycle : ILifeCycle {
    val dispose = CompositeDisposable()

    override var disposer: Disposable
        get() = dispose
        set(value) { dispose.add(value)}



    protected var lifecycleSubject = BehaviorSubject.create<Boolean>()

    override fun <T> bindToLifecycle(): LifecycleTransformer<T> {
        return LifecycleTransformer()
    }

    override fun stop() {
        lifecycleSubject.onNext(true)
        lifecycleSubject.onComplete()
        dispose.dispose()
    }


    inner class LifecycleTransformer<T> : ObservableTransformer<T, T>, SingleTransformer<T, T> {

        override fun apply(upstream: Observable<T>): ObservableSource<T> {
            return upstream.takeUntil(lifecycleSubject)
        }

        override fun apply(upstream: Single<T>): SingleSource<T> {
            return upstream.takeUntil(lifecycleSubject.firstOrError())
        }
    }

}
