package com.tassta.flex.utils

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.PublishSubject

open class Presenter : LifeCycle(), IPresenter {
    private val lifecycleViewSubject = PublishSubject.create<Any>()

    override fun <T> bindToViewLifecycle(view: Any): LifecycleViewTransformer<T> {
        return LifecycleViewTransformer(view)
    }

    override fun <_VIEW : Any> unbindView(view: _VIEW) {
        lifecycleViewSubject.onNext(view)
    }
//    override fun unbindView(view: Any) {
//        lifecycleViewSubject.onNext(view)
//    }

    override fun stop() {
        super.stop()
        lifecycleViewSubject.onComplete()
    }

    inner class LifecycleViewTransformer<T> internal constructor(private val view: Any) : ObservableTransformer<T, T> {

        override fun apply(upstream: Observable<T>): ObservableSource<T> {
            return upstream
                    .takeUntil(lifecycleSubject)
                    .takeUntil(lifecycleViewSubject
                            .filter { o -> view === o }
                    )
        }
    }
}

