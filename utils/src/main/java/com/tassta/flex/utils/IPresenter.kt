package com.tassta.flex.utils

interface IPresenter : ILifeCycle {
    fun <T> bindToViewLifecycle(view: Any): Presenter.LifecycleViewTransformer<T>
    fun <_VIEW : Any> unbindView(view: _VIEW)
}