package com.tassta.flex.utils

import io.reactivex.*
import java.util.concurrent.TimeUnit

fun repeatIn(seconds:Long, times:Long): (Observable<Throwable>) -> Observable<Long>
        = { it.take(times).flatMap { Observable.timer(seconds, TimeUnit.SECONDS) } }
fun repeatIn(seconds:Long, transformer:ObservableTransformer<Throwable,Throwable>): (Observable<Throwable>) -> Observable<Long>
        = { it.compose(transformer).flatMap { Observable.timer(seconds, TimeUnit.SECONDS) } }
fun <T>repeatIn(seconds:Long, lifeCycle: ObservableSource<T>): (Observable<Throwable>) -> Observable<Long>
        = { it.takeUntil(lifeCycle).flatMap { Observable.timer(seconds, TimeUnit.SECONDS) } }

fun <T>Observable<T>.immediately(): Maybe<T> = timeout(100, TimeUnit.MILLISECONDS, Observable.empty<T>()).firstElement()
fun <T>Maybe<T>.immediately(): Maybe<T> = timeout(100, TimeUnit.MILLISECONDS, Maybe.empty<T>())
fun <T>Single<T>.immediately(): Single<T> = timeout(100, TimeUnit.MILLISECONDS, Single.error(NoSuchElementException()))
