package com.tassta.flex.utils

import io.reactivex.disposables.Disposable

/**
 * Interface provide RxLifecycle. Observables will be complete, while stpo method is called.<br></br>
 * Usage : Observable.compose(bindToLifecycle()).subscribe();<br></br><br></br>
 * Created by dmitry subbotenko on 05.05.2017.
 */

interface ILifeCycle {

    /**
     * Lifecycle transformer to complete observer while stop();<br></br>
     * Usage : Observable.compose(bindToLifecycle()).subscribe();<br></br>
     * @return transformer
     */
    fun <T> bindToLifecycle(): LifeCycle.LifecycleTransformer<T>

    /**
     * Stop aff lifecycle observables.
     */
    fun stop()

    var disposer:Disposable
}
