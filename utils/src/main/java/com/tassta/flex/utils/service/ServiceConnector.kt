package com.tassta.flex.utils.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import io.reactivex.Maybe
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.BehaviorSubject

/**
 * Connector base class to support service connection.
 */
internal class ServiceConnector<T : BoundService>
/**
 * Base class constructor.<br></br><br></br>
 *
 *
 * Returns an object, that's provide access to service.<br></br>
 *
 * @param context   Context ot service binding.
 * @param clazz     Class of the service
 * @param lifeCycle Observable transformer, that stop binding service, while it is not needed
 * You can use [kris.com.utils.presenter.LifeCycle] class from this package, or trello/RxLifecycle package.
 */
 constructor(context: Context, clazz: Class<T>, lifeCycle: ObservableTransformer<T, T>? = null) : IServiceConnector<T> {

    private val serviceSubject = BehaviorSubject.create<T>()
    private val boundServiceConnection = BoundServiceConnection()

    /**
     * Return Observable that return just 1 instance of the service, and complete.
     * @return observable of service.
     */
    override val service: Maybe<T>
        get() = serviceSubject.firstElement()

    init {
        var lifeCycle = lifeCycle

        if (lifeCycle == null) {
            lifeCycle = ObservableTransformer { it }
        }

        val orderServiceIntent = Intent(context, clazz)
        context.bindService(orderServiceIntent,
                boundServiceConnection, Context.BIND_AUTO_CREATE)

        serviceSubject
                .compose(lifeCycle)
                .subscribe(
                        {},
                        {it.printStackTrace()},
                {context.unbindService(boundServiceConnection)})
    }

    /**
     * Unbind service, stop serviceConnector and complete it. This object can't be set again.
     */
    override fun complete() {
        serviceSubject.onComplete()
    }

    private inner class BoundServiceConnection : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            val service = (binder as ServiceBinder).service
            @Suppress("UNCHECKED_CAST")
            serviceSubject.onNext(service as T) // May produce Class cast Exception
        }

        override fun onServiceDisconnected(name: ComponentName) {
            serviceSubject.onComplete()
        }

    }
}
/**
 * Base class constructor.<br></br><br></br>
 *
 *
 * Returns an object, that's provide access to service.<br></br>
 *
 * @param context   Context ot service binding.
 * @param clazz     Class of the service
 */