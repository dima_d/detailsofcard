package com.tassta.flex.utils.service

import io.reactivex.Maybe

interface IServiceConnector<T : BoundService> {
    val service: Maybe<T>
    fun complete()
}
