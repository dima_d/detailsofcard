package kris.com.model

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import java.util.*

@Table(database = AppDatabase::class)
data class CardOperation(
        @PrimaryKey var id: Int = 0,
        @Column var cardNumber:String = "",
        @Column var date: Date = Date(),
        @Column var purchase: Float = 0f,
        @Column var ballance: Float = 0f,
        @Column var operationType: OperationType = OperationType.OUTCOME,
        @Column var shop: String = ""
)

enum class OperationType{
    INCOME,
    OUTCOME
}