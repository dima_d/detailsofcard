package kris.com.model

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.Toast
import com.raizlabs.android.dbflow.kotlinextensions.save
import com.zdvdev.kursor.forEach
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.regex.Pattern

class SmsImporter(context: Context){

    private val ctx = WeakReference(context)

    companion object {
        const val INBOX = "content://sms/inbox"
        const val SENT = "content://sms/sent"
        const val DRAFT = "content://sms/draft"
        private val TAG = SmsImporter::class.java.simpleName
    }

    val pattern = Pattern.compile("""Карта (\d{4})([ \d.:]*)(\D*)([\d.]*) (\S*)\D*([\d.]*) (\S*)(.*)""")
    val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")

//    Карта 4038 15.06.2018 21:28 Покупка 57.00 RUB. Доступно: 57502.89 RUB MAGAZIN N34 ST.-PETERSBUR
//    Карта 4038 15.06.2018 21:17 Покупка 49.00 RUB. Доступно: 57559.89 RUB MAGAZIN N169 ST.-PETERSBUR
//    Карта 4038 15.06.2018 15:50 Покупка 238.60 RUB. Доступно: 57608.89 RUB POLUSHKA SANKT-PETERBU
//    Карта 4038 15.06.2018 11:53 Баланс карты увеличен на 42000.00 RUB. Доступно: 57847.49 RUB
//    Карта 4038 08.06.2018 10:21 Выдача средств 30000.00 RUB. Доступно: 24389.21 RUB ATM1031 PROSPEKT SLAVY ST.PETERSBURG
//    86 UniCredit 1528442491375 Карта 4038 08.06.2018 10:21 Выдача средств 30000.00 RUB. Доступно: 24389.21 RUB ATM1031 PROSPEKT SLAVY ST.PETERSBURG

    fun import(){
        ctx.get()?.let {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && it.checkSelfPermission(Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(it,it.getString(R.string.permissiom_sms_needed),Toast.LENGTH_LONG).show()
                return
            }

            val cursor = it.getContentResolver().query(Uri.parse(INBOX), null, """ address like "UniCredit" """, null, null)
            cursor.use{
                val id     = cursor.getColumnIndex("_id")
//                val address = cursor.getColumnIndex("address")
//                val date    = cursor.getColumnIndex("date")
                val body    = cursor.getColumnIndex("body")

                it.forEach {

                    val matcher = pattern.matcher(it.stringAt(body))
                    if(matcher.matches()){
                        val cardOperation = CardOperation(
                                id = it.intAt(id),
                                cardNumber = matcher.group(1),
                                date = dateFormat.parse(matcher.group(2)),
                                operationType = if (matcher.group(3).contains("увеличен", true)) OperationType.INCOME else OperationType.OUTCOME,
                                purchase = matcher.group(4).toFloat(),
                                ballance = matcher.group(6).toFloat(),
                                shop = matcher.group(8)
                        ).apply { save() }
                        Log.d(TAG, "import() $cardOperation");
                    }
                }
            }

        }
    }

}