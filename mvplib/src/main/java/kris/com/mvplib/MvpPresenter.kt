package kris.com.mvplib

interface IMvpPresenter<_VIEW:IMvpView> {
    abstract fun attachView(view:_VIEW)
    abstract fun detachView(view:_VIEW)


}